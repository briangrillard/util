##Well well well, lets install opencv

mkdir OpenCV
cd OpenCV

echo "Installing Dependences"
sudo apt-get install -y pkgconfig
sudo apt-get install -y python-dev
sudo apt-get install -y python-numpy
sudo apt-get install -y ffmpeg
sudo apt-get install -y libavcodec-dev
sudo apt-get install -y libavformat-dev
sudo apt-get install -y libswscale-dev
sudo apt-get install -y libjpeg-dev
sudo apt-get install -y libpng-dev
sudo apt-get install -y libtiff-dev
sudo apt-get install -y libjasper-dev

##sudo apt-get install libav
##sudo apt-get install libavcodec-dev
##sudo apt-get install libavformat-dev
##sudo apt-get install libswscale-dev
##sudo apt-get install libjpeg-dev
##sudo apt-get install libpng-dev
##sudo apt-get install libtiff-dev 
##sudo apt-get install libjasper-dev.

echo "Downloadng opencv"
wget -O OpenCV-2.4.7.tar.gz http://sourceforge.net/projects/opencvlibrary/files/opencv-unix/2.4.7/opencv-2.4.7.tar.gz/download

echo "installing opencv"
tar -xf OpenCV-2.4.7.tar.gz

cd opencv-2.4.7

mkdir build
cd build

cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local ..
##cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_TBB=ON -D BUILD_NEW_PYTHON_SUPPORT=ON -D WITH_V4L=ON -D INSTALL_C_EXAMPLES=ON -D INSTALL_PYTHON_EXAMPLES=ON -D BUILD_EXAMPLES=ON -D WITH_QT=ON -D WITH_OPENGL=ON ..

make
sudo make install
sudo sh -c 'echo "/usr/local/lib" > /etc/ld.so.conf.d/opencv.conf'
sudo ldconfig

echo "End of installing opencv, hoped it worked well for you, now lets Babel !"
