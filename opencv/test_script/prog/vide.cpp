//
// vide.cpp for capt video in /home/grilla_b//Documents/perso/opencv
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Sat May 18 22:47:41 2013 brian grillard
// Last update Fri Nov 29 14:57:42 2013 brian grillard
//

#include <stdio.h>
#include <highgui.h>
#include <cv.h>

#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h> 
#include <netdb.h>
#include <arpa/inet.h>
#include <vector>

struct addrinfo _addr;
int _sock;

struct sockaddr_in _addr_in;

void		snd()
{
  _addr.ai_family = AF_INET;
  _addr.ai_addrlen = sizeof(_addr_in);
  _addr.ai_socktype = SOCK_DGRAM;
  _addr.ai_protocol = IPPROTO_UDP;
  _addr.ai_flags = AI_PASSIVE;
  _addr.ai_addr = (sockaddr *)&_addr_in;

  _addr_in.sin_family = _addr.ai_family;
  _addr_in.sin_addr.s_addr = htonl(INADDR_ANY);
  _addr_in.sin_port = htons(4243);
  if ((_sock = socket(_addr.ai_family, _addr.ai_socktype, _addr.ai_protocol))
      == -1)
    {
      std::cerr << "can not create socket" << std::endl;
    }

  struct sockaddr_in addr;
  socklen_t          addr_size;

  addr_size = sizeof(struct sockaddr_in);
  addr.sin_addr.s_addr = inet_addr("127.0.0.1");
  addr.sin_family = AF_INET;
  addr.sin_port = htons(4244);

  cv::VideoCapture cap(0);

  //  std::cout << cap.get(CV_CAP_PROP_FRAME_WIDTH) << " : "<< cap.get(CV_CAP_PROP_FRAME_HEIGHT) << std::endl;

  int ret = 0;

  //(1) jpeg compression
  std::vector<uchar> buff;//buffer for coding
  std::vector<int> param = std::vector<int>(2);
  cv::Mat image;
  std::string file = "vido.avi";

  //cv::VideoWriter vw;

  //  vw.open(file, CV_FOURCC('P','I','M', '1'), 30.0, cv::Size(640, 480),true);
  /*if (vw.isOpened())
    std::cout << "Video is opend !" << std::endl;
  else
    std::cout << "Video is not opend !" << std::endl;
  */
  //cv::namedWindow("Video", CV_WINDOW_AUTOSIZE);

  while (1)
    {
      //      i++;
      param[0]=CV_IMWRITE_JPEG_QUALITY;
      param[1]=60;//default(95) 0-100
      cap >> image; 
      /*      vw << image;
      if ((imencode(".jpg", image, buff, param)) == 0)
      std::cout << " FAIL " << std::endl;*/
      std::string t;
      int i = 0;
      for (; i != (int)buff.size(); ++i)
	{
	  uchar a = 0;
	  a = buff[i];
	  t += a;
	}
      //      std::cout << t.size() << std::endl;
    std::cout << "A" << std::endl;
      if ((ret = sendto(_sock, t.c_str(), t.size(), 0, 
			(const sockaddr *)&addr, addr_size)) <= (ssize_t)0)
	perror(NULL);
    std::cout << "P" << std::endl;
      t.erase(t.begin(), t.end());
    }
}

struct s_tot
{
  int i;
  char t[512];
};

int		main(int argc, char **av)
{
  //  struct s_tot i;

  //std::cout << sizeof(i) << std::endl;
  //  snd();
  /*  
  cv::VideoCapture cap(0);

  if(!cap.isOpened())
    {
      printf("couldnt open video capture\n");
      return -1;
    }


  //createTrackbar("quality","jpg",&q,100);


  std::vector<uchar> buff;//buffer for coding
  std::vector<int> param = std::vector<int>(2);
  cv::Mat image;
  cv::namedWindow("Video", CV_WINDOW_AUTOSIZE);
  while (1)
    {
      cv::Mat show = imdecode(cv::Mat(buff),CV_LOAD_IMAGE_COLOR);



      cv::imshow("Video", show);
      if(cv::waitKey(10) == 99 ) break;
    }
  */

  //    snd();
  
  std::cout << "A" << std::endl;
  cv::VideoCapture cap(0);
  //  cap.set(CV_CAP_PROP_FRAME_WIDTH, 1024);
  //cap.set(CV_CAP_PROP_FRAME_HEIGHT, 768);
  std::cout << "B" << std::endl;
  //  cap.set(CV_CAP_PROP_FRAME_WIDTH, 860);  
  //cap.set(CV_CAP_PROP_FRAME_HEIGHT, 640);       
  if(!cap.isOpened()) 
    {
      printf("couldnt open video capture\n");
      return -1;
    }
     std::cout << "C" << std::endl;    
  cv::Mat image;
  //cv::Mat image2;
  cv::namedWindow("Video", 0);
  //char buff[512];
     std::cout << "D" << std::endl;    
  while(1)
    {

      // cap.set(CV_CAP_PROP_FRAME_WIDTH, 160);
      //cap.set(CV_CAP_PROP_FPS , 120.1);
      //      cap.set( CV_CAP_OPENNI_IMAGE_GENERATOR_OUTPUT_MODE, CV_CAP_OPENNI_VGA_30HZ );

      cap >> image;
      //      cv::resize(image, 100.0, 100.0);
      

      //  std::cout << image.total() << std::endl;
      //memcpy(buff, &image, sizeof(image));
      //buff[sizeof(image)] = '\0';

      //memcpy(&image2, buff, sizeof(image));

      cv::imshow("Video", image);

      //if(cv::waitKey(10) == 99 ) break;
    }
  return (0);
}
