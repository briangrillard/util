======== require.sh ======

This script just perform to install and update some basic libs which are useful to install all opencv's dependencies, like cmake and pkg-config. (if you just redump, there's the build-essential installer)
So just have a look at this script and edit for your need. (it doesn't need to be run if you already have all what it install)

======= opencv.sh =======

This script downloads and installs opencv-2.4.7 and all its dependencies, it has been test only on debian squeezy but in theory it should work well on others distributions.
After downloading opencv, cmake installs it and link all external libs. 
